import junit.framework.TestCase;

public class StackTest extends TestCase {
	public void testOneItem() {
		Stack<Integer> s = new Stack<Integer>();
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
		s.push(Integer.valueOf(1));
		assertEquals(Integer.valueOf(1), s.top());
		assertEquals(Integer.valueOf(1), s.popoff());
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
	}
	
	public void testTwoItems() {
		Stack<Integer> s = new Stack<Integer>();
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
		s.push(Integer.valueOf(1));
		assertEquals(Integer.valueOf(1), s.top());
		s.push(Integer.valueOf(2));
		assertEquals(Integer.valueOf(2), s.top());
		assertEquals(Integer.valueOf(2), s.popoff());
		assertEquals(Integer.valueOf(1), s.top());
		assertEquals(Integer.valueOf(1), s.popoff());
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
	}
	
	public void testThreeItems() {
		Stack<Integer> s = new Stack<Integer>();
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
		s.push(Integer.valueOf(1));
		assertEquals(Integer.valueOf(1), s.top());
		s.push(Integer.valueOf(2));
		assertEquals(Integer.valueOf(2), s.top());
		s.push(Integer.valueOf(3));
		assertEquals(Integer.valueOf(3), s.top());
		assertEquals(Integer.valueOf(3), s.popoff());
		assertEquals(Integer.valueOf(2), s.top());
		assertEquals(Integer.valueOf(2), s.popoff());
		assertEquals(Integer.valueOf(1), s.top());
		assertEquals(Integer.valueOf(1), s.popoff());
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
	}
	
	public void testAddAfterRemove() {
		Stack<Integer> s = new Stack<Integer>();
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
		s.push(Integer.valueOf(1));
		assertEquals(Integer.valueOf(1), s.top());
		s.push(Integer.valueOf(2));
		assertEquals(Integer.valueOf(2), s.top());
		assertEquals(Integer.valueOf(2), s.popoff());
		assertEquals(Integer.valueOf(1), s.top());
		s.push(Integer.valueOf(3));
		assertEquals(Integer.valueOf(3), s.top());
		assertEquals(Integer.valueOf(3), s.popoff());
		assertEquals(Integer.valueOf(1), s.top());
		assertEquals(Integer.valueOf(1), s.popoff());
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
		assertEquals(null, s.top());
		assertEquals(null, s.popoff());
	}
	
	public void testAddNull() {
		NullPointerException got = null;
		Stack<Integer> s = new Stack<Integer>();
		try {
			s.push(null);
		} catch (NullPointerException e) {
			got = e;
		}
		assertFalse(got == null);
	}
}
