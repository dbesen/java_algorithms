import junit.framework.TestCase;

public class NextNodeTest extends TestCase {
	public void testEquals() {
		NextNode<Integer> a = new NextNode<Integer>(Integer.valueOf(1));
		NextNode<Integer> b = new NextNode<Integer>(Integer.valueOf(1));
		NextNode<Integer> c = new NextNode<Integer>(Integer.valueOf(2));
		NextNode<Boolean> d = new NextNode<Boolean>(Boolean.valueOf(true));
		// leave a's next null
		b.setNext(c);
		assertFalse(a.equals(null));
		assertFalse(a.equals(Integer.valueOf(1)));
		assertFalse(a.equals(Boolean.valueOf(true)));
		assertEquals(a, b);
		assertFalse(a.equals(c));
		assertFalse(c.equals(a));
		assertFalse(b.equals(c));
		assertFalse(c.equals(b));
		assertFalse(a.equals(d));
		assertFalse(d.equals(a));
	}
	public void testNextNode() {
		NextNode<Integer> node = new NextNode<Integer>(Integer.valueOf(3));
		assertEquals(Integer.valueOf(3), node.getItem());
		assertEquals(null, node.getNext());
		assertEquals(Integer.valueOf(3), node.getItem());
		node.setNext(node);
		assertEquals(node, node.getNext());
		node.setNext(null);
		assertEquals(null, node.getNext());
		NextNode<Integer> node2 = new NextNode<Integer>(Integer.valueOf(2));
		assertEquals(Integer.valueOf(2), node2.getItem());
		node.setNext(node2);
		node2.setNext(node);
		assertEquals(node2, node.getNext());
		assertEquals(node, node2.getNext());
		assertEquals(node, node.getNext().getNext());
	}
}
