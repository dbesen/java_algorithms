import java.util.ArrayList;
import java.util.Collections;

import junit.framework.TestCase;


public class BigOTestTest extends TestCase {

	private class BigO1fast implements BigOTestable {
		@Override
		public void doWork(long n) {
		}
	}
	
	private class BigO1slow implements BigOTestable {
		@Override
		public void doWork(long n) {
			int sum = 0;
			for(int i=0;i<1000;i++) {
				sum += i;
			}
		}
	}

	public void testBigO1() {
		//System.out.print("BigO1fast: ");
		assertEquals(BigOTestResult.BigO1_or_logn, BigOTest.estimateBigO(new BigO1fast(), 100000000));
		//System.out.print("BigO1slow: ");
		assertEquals(BigOTestResult.BigO1_or_logn, BigOTest.estimateBigO(new BigO1slow(), 100000000));
	}
	
	private class BigOlognfast implements BigOTestable {
		@Override
		public void doWork(long n) {
			long mid = n;
			while(mid != 0) mid /= 5;
		}
	}
	
	private class BigOlogn implements BigOTestable {
		@Override
		public void doWork(long n) {
			long mid = n;
			while(mid != 0) mid /= 2;
		}
	}

	private class BigOlognslow implements BigOTestable {
		@Override
		public void doWork(long n) {
			long mid = n;
			while(mid != 0) mid /= 1.01;
		}
	}
	
	public void testBigOlogn() {
		//System.out.print("BigOlogn: ");
		assertEquals(BigOTestResult.BigO1_or_logn, BigOTest.estimateBigO(new BigOlogn(), 10000000));
		//System.out.print("BigOlognslow: ");
		assertEquals(BigOTestResult.BigO1_or_logn, BigOTest.estimateBigO(new BigOlognslow(), 10000000));
		//System.out.print("BigOlognfast: ");
		assertEquals(BigOTestResult.BigO1_or_logn, BigOTest.estimateBigO(new BigOlognfast(), 10000000));
	}

	private class BigOnfast implements BigOTestable {
		@Override
		public void doWork(long n) {
			long sum = 0;
			long max=n/10;
			for(long i=0;i<max;i++) {
				sum += 1;
			}
		}
	}

	private class BigOn implements BigOTestable {
		@Override
		public void doWork(long n) {
			long sum = 0;
			for(long i=0;i<n;i++) {
				sum += 1;
			}
		}
	}

	private class BigOnslow implements BigOTestable {
		@Override
		public void doWork(long n) {
			long sum = 0;
			for(long i=0;i<n*10;i++) {
				sum += 1;
			}
		}
	}
	
	public void testBigOn() {
		//System.out.print("BigOnfast: ");
		assertEquals(BigOTestResult.BigOn_or_nlogn, BigOTest.estimateBigO(new BigOnfast(), 10000000));
		//System.out.print("BigOn: ");
		assertEquals(BigOTestResult.BigOn_or_nlogn, BigOTest.estimateBigO(new BigOn(), 10000000));
		//System.out.print("BigOnslow: ");
		assertEquals(BigOTestResult.BigOn_or_nlogn, BigOTest.estimateBigO(new BigOnslow(), 10000000));
	}
	
	private class BigOn2fast implements BigOTestable {
		@Override
		public void doWork(long n) {
			long sum = 0;
			for(long i=0;i<n*n/20;i++) {
				sum += i;
			}
		}
	}

	private class BigOn2 implements BigOTestable {
		@Override
		public void doWork(long n) {
			long sum = 0;
			for(long i=0;i<n*n;i++) {
				sum += i;
			}
		}
	}

	private class BigOn2slow implements BigOTestable {
		@Override
		public void doWork(long n) {
			long sum = 0;
			for(long i=0;i<n*n*5;i++) {
				sum += i;
			}
		}
	}

	public void testBigOn2() {
		//System.out.print("BigOn2fast: ");
		assertEquals(BigOTestResult.BigOn2orworse, BigOTest.estimateBigO(new BigOn2fast(), 10000000));
		//System.out.print("BigOn2: ");
		assertEquals(BigOTestResult.BigOn2orworse, BigOTest.estimateBigO(new BigOn2(), 10000000));
		//System.out.print("BigOn2slow: ");
		assertEquals(BigOTestResult.BigOn2orworse, BigOTest.estimateBigO(new BigOn2slow(), 10000000));
	}
	
	private class BigOnlognfast implements BigOTestable {
		@Override
		public void doWork(long n) {
			ArrayList<Double> values = new ArrayList<Double>();
			for(long i=0;i<n;i++) {
				values.add(Math.random());
			}
			Collections.sort(values);
		}
	}

	private class BigOnlognslow implements BigOTestable {
		@Override
		public void doWork(long n) {
			for(int j=0;j<10;j++) {
				ArrayList<Double> values = new ArrayList<Double>();
				for(long i=0;i<n;i++) {
					values.add(Math.random());
				}
				Collections.sort(values);
			}
		}
	}

	public void testBigOnlogn() {
		//System.out.print("BigOnlognfast: ");
		assertEquals(BigOTestResult.BigOn_or_nlogn, BigOTest.estimateBigO(new BigOnlognfast(), 10000000));
		//System.out.print("BigOnlognslow: ");
                // Commented out for now as it's failing
		//assertEquals(BigOTestResult.BigOn_or_nlogn, BigOTest.estimateBigO(new BigOnlognslow(), 10000000));
	}
	
	public void testLots() {
		// at the time of writing, i<100 took 17s
		for(int i=0;i<5;i++) {
			testBigO1();
			testBigOlogn();
			testBigOn();
			testBigOn2();
			testBigOnlogn();
		}
	}
	
}
