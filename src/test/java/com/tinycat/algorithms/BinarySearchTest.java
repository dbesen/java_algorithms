import junit.framework.TestCase;

public class BinarySearchTest extends TestCase {

	public void testSomething() {
		int result = BinarySearch.get(0, 50, new BinarySearchPredicate() { public boolean test(int index) { return index >= 23; } } );
		assertEquals(23, result);
		result = BinarySearch.get(0, 50, new BinarySearchPredicate() { public boolean test(int index) { return false; } } );
		assertEquals(51, result);
		result = BinarySearch.get(0, 50, new BinarySearchPredicate() { public boolean test(int index) { return true; } } );
		assertEquals(0, result);
	}
	
	public void testBigO() {
		BigOTestable bt = new BigOTestable() {
			public void doWork(long n) {
				BinarySearch.get(0, (int) n, new BinarySearchPredicate() { public boolean test(int index) { return index >= 23; } });
			}
		};
		assertEquals(BigOTestResult.BigO1_or_logn, BigOTest.estimateBigO(bt, 1000000000));
	}
	
}