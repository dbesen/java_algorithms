import java.math.BigInteger;

import junit.framework.TestCase;


public class AckermannTest extends TestCase {
	public void testVals() {
		assertEquals(0, Ackermann.getAckermann(0, -1));
		assertEquals(0, Ackermann.getAckermann(-1, 0));
		assertEquals(0, Ackermann.getAckermann(-1, -1));

		assertEquals(1, Ackermann.getAckermann(0, 0));
		assertEquals(2, Ackermann.getAckermann(0, 1));
		assertEquals(3, Ackermann.getAckermann(0, 2));
		assertEquals(4, Ackermann.getAckermann(0, 3));
		assertEquals(5, Ackermann.getAckermann(0, 4));
		
		assertEquals(2, Ackermann.getAckermann(1, 0));
		assertEquals(3, Ackermann.getAckermann(1, 1));
		assertEquals(4, Ackermann.getAckermann(1, 2));
		assertEquals(5, Ackermann.getAckermann(1, 3));
		assertEquals(6, Ackermann.getAckermann(1, 4));
		
		assertEquals(3, Ackermann.getAckermann(2, 0));
		assertEquals(5, Ackermann.getAckermann(2, 1));
		assertEquals(7, Ackermann.getAckermann(2, 2));
		assertEquals(9, Ackermann.getAckermann(2, 3));
		assertEquals(11, Ackermann.getAckermann(2, 4));
		
		assertEquals(5, Ackermann.getAckermann(3, 0));
		assertEquals(13, Ackermann.getAckermann(3, 1));
		assertEquals(29, Ackermann.getAckermann(3, 2));
		assertEquals(61, Ackermann.getAckermann(3, 3));
		assertEquals(125, Ackermann.getAckermann(3, 4));
		
		assertEquals(13, Ackermann.getAckermann(4, 0));
		
		assertEquals(65533, Ackermann.getAckermann(4, 1));
		assertEquals(65533, Ackermann.getAckermann(5, 0));
		
		assertEquals(new BigInteger("85720688574901385675874003924800144844912384936442688595500031069628084089994889799455870305255668650207573833404251746014971622855385123487876620597588598431476542198593847883368596840498969135023633457224371799868655530139190140473324351568616503316569571821492337341283438653220995094697645344555005"),
				Ackermann.getAckermann(BigInteger.valueOf(3), BigInteger.valueOf(1000)));
		assertFalse(new BigInteger("5").equals(Ackermann.getAckermann(BigInteger.valueOf(3), BigInteger.valueOf(1000))));
	}
}
