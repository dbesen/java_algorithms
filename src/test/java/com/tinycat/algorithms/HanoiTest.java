import junit.framework.TestCase;


public class HanoiTest extends TestCase {
	private void assert2DIntArrayEquals(int[][] a, int[][] b) {
		assertEquals(a.length, b.length);
		for(int i=0;i<a.length;i++) {
			assertEquals(a[i].length, b[i].length);
			for(int j=0;j<a[i].length;j++) {
				assertEquals(b[i][j], a[i][j]);
			}
		}
	}
	
	public void testInvalid() {
		assertEquals(null, Hanoi.getSolutionAsString(0));
		assertEquals(null, Hanoi.getSolutionAsString(-1));
		assertEquals(null, Hanoi.getSolutionAsString(-47));
		
		assertEquals(null, Hanoi.getSolutionAsIntArray(0));
		assertEquals(null, Hanoi.getSolutionAsIntArray(-1));
		assertEquals(null, Hanoi.getSolutionAsIntArray(-113));
	}
	
	public void testOneDisc() {
		String t = Hanoi.getSolutionAsString(1);
		assertEquals("Move a disk from 0 to 2\n", t);

		int[][] s = Hanoi.getSolutionAsIntArray(1);
		int[][] expectedSolution = new int[1][2];
		expectedSolution[0][0] = 0; // first move from tower
		expectedSolution[0][1] = 2; // first move to tower
		assert2DIntArrayEquals(s, expectedSolution);
	}
	
	public void testTwoDiscs() {
		String t = Hanoi.getSolutionAsString(2);
		assertEquals("Move a disk from 0 to 1\nMove a disk from 0 to 2\nMove a disk from 1 to 2\n", t);		

		int[][] s = Hanoi.getSolutionAsIntArray(2);
		int[][] expectedSolution = new int[3][2];
		expectedSolution[0][0] = 0; // first move from tower
		expectedSolution[0][1] = 1; // first move to tower
		expectedSolution[1][0] = 0;
		expectedSolution[1][1] = 2;
		expectedSolution[2][0] = 1;
		expectedSolution[2][1] = 2;
		assert2DIntArrayEquals(s, expectedSolution);
	}

	public void testThreeDiscs() {
		String t = Hanoi.getSolutionAsString(3);
		assertEquals("Move a disk from 0 to 2\nMove a disk from 0 to 1\nMove a disk from 2 to 1\nMove a disk from 0 to 2\nMove a disk from 1 to 0\nMove a disk from 1 to 2\nMove a disk from 0 to 2\n", t);		

		int[][] s = Hanoi.getSolutionAsIntArray(3);
		int[][] expectedSolution = new int[7][2];
		expectedSolution[0][0] = 0; // first move from tower
		expectedSolution[0][1] = 2; // first move to tower
		expectedSolution[1][0] = 0;
		expectedSolution[1][1] = 1;
		expectedSolution[2][0] = 2;
		expectedSolution[2][1] = 1;
		expectedSolution[3][0] = 0;
		expectedSolution[3][1] = 2;
		expectedSolution[4][0] = 1;
		expectedSolution[4][1] = 0;
		expectedSolution[5][0] = 1;
		expectedSolution[5][1] = 2;
		expectedSolution[6][0] = 0;
		expectedSolution[6][1] = 2;
		assert2DIntArrayEquals(s, expectedSolution);
	}
}
