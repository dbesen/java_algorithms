import junit.framework.TestCase;

public class QueueTest extends TestCase {
	public void testOneItem() {
		Queue<Integer> q = new Queue<Integer>();
		assertEquals(null, q.front());
		q.add(3);
		assertEquals(Integer.valueOf(3), q.front());
		assertEquals(Integer.valueOf(3), q.remove());
		assertEquals(null, q.front());
		assertEquals(null, q.remove());
		assertEquals(null, q.front());
		assertEquals(null, q.remove());
	}
	
	public void testTwoItems() {
		Queue<Integer> q = new Queue<Integer>();
		assertEquals(null, q.front());
		q.add(1);
		assertEquals(Integer.valueOf(1), q.front());
		q.add(2);
		assertEquals(Integer.valueOf(1), q.front());
		assertEquals(Integer.valueOf(1), q.remove());
		assertEquals(Integer.valueOf(2), q.front());
		assertEquals(Integer.valueOf(2), q.remove());
		assertEquals(null, q.front());
		assertEquals(null, q.remove());
		assertEquals(null, q.front());
		assertEquals(null, q.remove());
	}
	
	public void testThreeItems() {
		Queue<Integer> q = new Queue<Integer>();
		assertEquals(null, q.front());
		q.add(1);
		assertEquals(Integer.valueOf(1), q.front());
		q.add(2);
		assertEquals(Integer.valueOf(1), q.front());
		q.add(3);
		assertEquals(Integer.valueOf(1), q.front());
		assertEquals(Integer.valueOf(1), q.remove());
		assertEquals(Integer.valueOf(2), q.front());
		assertEquals(Integer.valueOf(2), q.remove());
		assertEquals(Integer.valueOf(3), q.front());
		assertEquals(Integer.valueOf(3), q.remove());
		assertEquals(null, q.front());
		assertEquals(null, q.remove());
		assertEquals(null, q.front());
		assertEquals(null, q.remove());
	}
	
	public void testAddAfterRemove() {
		Queue<Integer> q = new Queue<Integer>();
		q.add(1);
		assertEquals(Integer.valueOf(1), q.front());
		q.add(2);
		assertEquals(Integer.valueOf(1), q.front());
		assertEquals(Integer.valueOf(1), q.remove());
		assertEquals(Integer.valueOf(2), q.front());
		q.add(3);
		assertEquals(Integer.valueOf(2), q.front());
		assertEquals(Integer.valueOf(2), q.remove());
		assertEquals(Integer.valueOf(3), q.front());
		assertEquals(Integer.valueOf(3), q.remove());
		assertEquals(null, q.front());
		assertEquals(null, q.remove());
		assertEquals(null, q.front());
		assertEquals(null, q.remove());
	}
	
	public void testAddNull() {
		NullPointerException got = null;
		Queue<Integer> q = new Queue<Integer>();
		try {
			q.add(null);
		} catch (NullPointerException e) {
			got = e;
		}
		assertFalse(got == null);
	}
}
