import static org.junit.Assert.*;
import org.junit.Test;


public class SpeedMeasurementConsistencyTest {
        private long doWork() {
                long count=0;
                for(long i=0;i<100;i++) {
                        count++;
                }
                return count;
        }

        private long timeIt() {
                long start = System.nanoTime();
                doWork();
                return System.nanoTime() - start;
        }

        private long getShortest(long[] results) {
                long shortest = -1;
                for(int i=0;i<results.length;i++) {
                        if(shortest < 0) shortest = results[i];
                        if(shortest > results[i]) shortest = results[i];
                }
                return shortest;
        }

        @Test
        public void testSmallVarianceBetweenTests() {
                long variance = 0;
                long lastVal = -1;
                int numResults = 100;
                for(int i=0;i<numResults;i++) {
                        long[] results = doRun();
                        long currVal = getShortest(results);
                        if(lastVal < 0) {
                                lastVal = currVal;
                        }
                        variance += Math.abs(lastVal - currVal);
                        lastVal = currVal;
                }
                variance /= numResults;

                // 50 nanosecond variance is pretty good, for an operation that takes about 1300...
                assertTrue("Variance should be small, was " + variance, variance < 50);
        }

        public long[] doRun() {
                int num_results = 100;
                long[] results = new long[num_results];
                for(int i=0;i<num_results;i++) {
                        results[i] = timeIt();
                }
                return results;
        }
}
