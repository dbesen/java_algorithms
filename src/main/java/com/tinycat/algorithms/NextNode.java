
public class NextNode<T> {
	// Note the item is immutable, but the pointer is not.
	private T item;
	private NextNode<T> next;
	public NextNode(T item) {
		this.item = item;
	}
	public T getItem() {
		return item;
	}
	public void setNext(NextNode<T> item) {
		next = item;
	}
	public NextNode<T> getNext() {
		return next;
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof NextNode<?>))
			return false;
		NextNode<?> other = (NextNode<?>) obj;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		return true;
	}
	
	public int hashCode() {
		return item.hashCode();
	}

}
