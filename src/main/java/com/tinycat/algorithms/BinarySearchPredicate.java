
// todo: BinarySearch should just have an interface classes can implement to be binary searchable, instead of having this weird predicate.
public abstract class BinarySearchPredicate {
	public abstract boolean test(int index);
}
