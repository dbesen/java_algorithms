
public class Stack<T> {
	private NextNode<T> head;

	public T top() {
		if(head == null) return null;
		return head.getItem();
	}

	public T popoff() {
		if(head == null) return null;
		T item = head.getItem();
		head = head.getNext();
		return item;
	}

	public void push(T item) {
		if(item == null) throw new NullPointerException("Attempting to add null item to stack");
		NextNode<T> node = new NextNode<T>(item);
		node.setNext(head);
		head = node;
	}

}
