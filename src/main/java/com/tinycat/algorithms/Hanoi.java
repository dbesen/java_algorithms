import java.util.ArrayList;


// How to test the Towers of Hanoi is an interesting engineering problem.  How do we represent the solution? 
// Another interesting problem is how to generalize this for any number of towers.
public class Hanoi {
	
	/**
	 * @param numTowers
	 * @param numDisks
	 * @return int[stepnum][0 is from, 1 is to]
	 */
	public static int[][] getSolutionAsIntArray(int numDisks) {
		if(numDisks <= 0) return null;
		Hanoi h = new Hanoi();
		ArrayList<int[]> soln = h.getSolution(numDisks);
		int[][] ret = new int[soln.size()][2];
		int place = 0;
		for(int[] step : soln) {
			ret[place] = step;
			place++;
		}
		return ret;
	}
	
	public static String getSolutionAsString(int numDisks) {
		if(numDisks <= 0) return null;
		Hanoi h = new Hanoi();
		ArrayList<int[]> soln = h.getSolution(numDisks);
		String ret = "";
		for(int[] step : soln) {
			ret += "Move a disk from " + step[0] + " to " + step[1] + "\n";
		}
		return ret;
	}
	
	private ArrayList<int[]> solution = new ArrayList<int[]>();
	
	private void reset() {
		solution = new ArrayList<int[]>();
	}
	
	private ArrayList<int[]> getSolution(int numDisks) {
		reset();
		hanoi(numDisks, 0, 2, 1);
		return solution;
	}
	
	// Yet another interesting problem would be how to do this with dynamic programming.
	// Keep in mind that hanoi(3, 0, 2, 1) is the same solution as hanoi(3, 2, 1, 0), just with numbers replaced.
	private void hanoi(int n, int source, int dest, int by) {
		if(n == 1) {
			// add step to solution
			int[] step = new int[2];
			step[0] = source;
			step[1] = dest;
			solution.add(step);
			return;
		}
		hanoi(n-1, source, by, dest);
		hanoi(1, source, dest, by);
		hanoi(n-1, by, dest, source);
	}

}
