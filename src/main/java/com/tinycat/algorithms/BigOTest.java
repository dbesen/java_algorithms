import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * This is not a test class.  It implements an algorithm to automatically detect what big O another algorithm is.
 * 
 */
public class BigOTest {
	
	public static final int runCountPerTest = 35;
	public static final int minRunCountPerTest = 30;
	
	public static LinkedHashMap<Long, Long> getData(BigOTestable target, long targetTime) {
		LinkedHashMap<Long, Long> shortestTimes = new LinkedHashMap<Long, Long>();
		LinkedHashMap<Long, Long> runCounts = new LinkedHashMap<Long, Long>();
		targetTime /= runCountPerTest;
		for(int i=0;i<runCountPerTest;i++) {
			long time = 0;
			long j=1;
			while(j <= 4 || (time < targetTime && j < Long.MAX_VALUE / 2)) {
				long runtime = getRuntime(target, j);
				time += runtime;
				setShortestTime(shortestTimes, j, runtime);
				
				Long c = runCounts.get(j);
				if(c == null) c = 0l;
				c++;
				runCounts.put(j, c);
				
				j *= 2;
			}
		}
		Long shortestRun = 0l;
		for(Map.Entry<Long, Long> curr : runCounts.entrySet()) {
			if(curr.getValue() >= minRunCountPerTest) {
				shortestRun = curr.getKey();
			}
		}
		//System.out.println("Shortest run is " + shortestRun);

		ArrayList<Long> toRemove = new ArrayList<Long>();
		for(Long key : shortestTimes.keySet()) {
			if(key > shortestRun) toRemove.add(key);
		}
		for(Long key : toRemove) {
			shortestTimes.remove(key);
		}
		
		return shortestTimes;
	}
	
	public static BigOTestResult estimateBigO(BigOTestable target, long targetTime) {
		return estimateBigO(getData(target, targetTime));
	}

	public static BigOTestResult estimateBigO(LinkedHashMap<Long, Long> data) {
		/*System.out.println();
		for(Map.Entry<Long, Long> curr : data.entrySet()) {
			System.out.println(curr.getKey() + " " + curr.getValue());
		}
		System.out.println();
		*/
		LinkedHashMap<Long, Double> a = new LinkedHashMap<Long, Double>();
		for(Map.Entry<Long, Long> curr : data.entrySet()) {
			a.put(curr.getKey(), (double) curr.getValue());
		}
		if(isHorizontal(a)) return BigOTestResult.BigO1_or_logn;

		LinkedHashMap<Long, Double> divn = new LinkedHashMap<Long, Double>();
		for(Map.Entry<Long, Long> curr : data.entrySet()) {
			divn.put(curr.getKey(), (double) curr.getValue() / curr.getKey());
		}
		if(isHorizontal(divn)) return BigOTestResult.BigOn_or_nlogn;
		
		LinkedHashMap<Long, Double> nlogn = new LinkedHashMap<Long, Double>();
		for(Map.Entry<Long, Long> curr : data.entrySet()) {
			nlogn.put(curr.getKey(), curr.getValue() / (curr.getKey() * Math.log(curr.getKey())));
		}
		if(isHorizontal(nlogn)) return BigOTestResult.BigOn_or_nlogn;
		
		return BigOTestResult.BigOn2orworse;
	}
	
	private static boolean isHorizontal(LinkedHashMap<Long, Double> data) {
		long lastKey = 1;
		double lastVal = 1;
		long prevKey = 1;
		double prevVal = 1;
		for(Map.Entry<Long, Double> curr : data.entrySet()) {
			prevKey = lastKey;
			prevVal = lastVal;
			lastKey = curr.getKey();
			lastVal = curr.getValue();
		}
		double slope = (lastVal - prevVal) / (lastKey - prevKey);
		//System.out.println("slope is " + slope);
		if(slope < .1) return true;
		return false;
	}
	
	private static void setShortestTime(LinkedHashMap<Long, Long> data, long which, long time) {
		Long curr = data.get(which);
		if(curr == null || curr > time) data.put(which, time);
	}
	
	private static long getRuntime(BigOTestable target, long n) {
		long time = System.nanoTime();
		target.doWork(n);
		return System.nanoTime() - time;
	}

}
