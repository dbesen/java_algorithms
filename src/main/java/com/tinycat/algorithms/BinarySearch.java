
public class BinarySearch {
	private BinarySearch() {} // static class
	public static int get(int minIndex, int maxIndex, BinarySearchPredicate pred) {
		// Recursion wastes stack space, so we do it iteratively.
		while(minIndex < maxIndex) {
			int center = (minIndex + maxIndex) / 2;
			if(pred.test(center)) {
				maxIndex = center;
			} else {
				minIndex = center + 1;
			}
		}
		if(!pred.test(minIndex)) return minIndex + 1;
		return minIndex;
	}
}
