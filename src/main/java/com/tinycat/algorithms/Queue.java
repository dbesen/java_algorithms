
public class Queue<T> {
	private NextNode<T> head;
	private NextNode<T> tail;

	public void add(T i) {
		if(i == null) throw new NullPointerException("Attempting to add a null item to queue");
		NextNode<T> node = new NextNode<T>(i);
		if(tail == null) {
			head = node;
			tail = node;
		} else {
			tail.setNext(node);
			tail = tail.getNext();
		}
	}

	public T front() {
		if(head == null) return null;
		return head.getItem();
	}

	public T remove() {
		if(head == null) return null;
		T item = front();
		head = head.getNext();
		if(head == null) tail = null;
		return item;
	}

}
