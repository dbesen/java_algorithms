import java.math.BigInteger;


public class Ackermann {
	// Perhaps this should be in a math library instead of in its own class?
	public static int getAckermann(int m, int n) {
		return getAckermann(BigInteger.valueOf(m), BigInteger.valueOf(n)).intValue();
	}
	
	public static long getAckermann(long m, long n) {
		return getAckermann(BigInteger.valueOf(m), BigInteger.valueOf(n)).longValue();
		
	}
	
	public static BigInteger getAckermann(BigInteger m, BigInteger n) {
		if(m.compareTo(BigInteger.valueOf(0)) < 0) return BigInteger.valueOf(0);
		if(m.compareTo(BigInteger.valueOf(0)) < 0) return BigInteger.valueOf(0);
		if(m.equals(BigInteger.valueOf(0))) return n.add(BigInteger.valueOf(1));
		
		// These two lines aren't in the definition, but reduce the stack usage significantly.
		if(m.equals(BigInteger.valueOf(1))) return n.add(BigInteger.valueOf(2));
		if(m.equals(BigInteger.valueOf(2))) return n.multiply(BigInteger.valueOf(2)).add(BigInteger.valueOf(3));
		
		if(n.equals(BigInteger.valueOf(0))) return getAckermann(m.subtract(BigInteger.valueOf(1)), BigInteger.valueOf(1));
		return getAckermann(m.subtract(BigInteger.valueOf(1)),
				getAckermann(m, n.subtract(BigInteger.valueOf(1))));
	}
}
